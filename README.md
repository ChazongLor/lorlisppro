This is the readme file for the project tictactoe

How to run a lisp file
-----------------------
1) Look up common lisp.
2) Click the second link with the url common-lisp.net.
3) Click "Get Started" button.
4) Click the download portacle button.
5) Click whichever download link you need. Depending on the machine you are using.
6) Once it is done downloading, find the file in your downloads folder
7) Double click on portacle.
8) Once it is fully loaded up. 
(type: (load "c:/Users/'name'/Downloads/tictactoe/titato.lisp")
9) After typing the above, read the instruction below.

How to play tic tac toe
------------------------
1) Next type (play)
2) A prompt will appear asking "If you would like to play against another player or a computer enter 1."
   with another prompt asking "If you would like to watch the computer play itself then press 2."
3) If you pressed 1 to choose between 2 modes, go see the instruction on
   "Choosing between player vs player or player vs computer". 
4) If you pressed 2, sit back and relax to see computer vs computer.

Choosing between player vs player or player vs computer
------------------------
1) 2 prompts will appear asking "Press 1 to play against another player." and
   "Press 2 to play against the computer."
2) If you pressed 1, go to step 3
   If you pressed 2, go to "Explaining player vs computer"
3) Once the game as started, another prompt will appear asking,
   "Please enter a move(0 through 8): "
4) If the number is less than 0 or greater than 8 it will show "Invalid input" and re-ask
   the prompt on step 4. If the number that is inputted not an empty space, it will show, 
   "Place is taken" and re-ask the prompt on step 4.
5) Keep playing until one wins or until it is a tie.

Explaining player vs computer
-----------------------------
Player vs Computer is the same as player vs player, it's just that the computer will make an
immediate move after you have inputted your number. Now go back to "Choosing between
player vs player or player vs computer" and go to step 3. 
