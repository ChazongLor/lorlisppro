;; titato.lisp
;;
;; Functions to play a game of tic-tac-toe
;; Chazong Lor
;;
;; list of indices for what the board looks like:
;; 0 1 2
;; 3 4 5
;; 6 7 8
;;

;;
(defun make-board ()
	(list '- '- '- '- '- '- '- '- '-)
)

;; Prints a tic-tac-toe board in a nice fashion.
;; Param: board - a list containing elements of a ttt board in row-major order
(defun print-board (board)
  (format t "=============")
  (do ((i 0 (+ i 1)) )
    ((= i 9) nil)
    (if (= (mod i 3) 0)
      (format t "~%|")
      nil)
    (format t " ~A |" (nth i board))
    )
  (format t "~%=============")
  (format t "~%")
)

;; Tests whether all three items are the same
(defun three-equal (list)
	(and (equal (first list) (second list))
		(equal (second list) (third list)))
)

;; Grabs the nth row of the tic-tac-toe board as a list
(defun grab-row (board row)
	(let ( (x (* 3 row)))
		(list (nth x board) (nth (+ 1 x) board) (nth (+ 2 x) board)))
)

;; Grabs the nth column of the tic-tac-toe board as a list
(defun grab-col (board col)
	(list (nth col board) (nth (+ 3 col) board) (nth (+ 6 col) board))
)

;; Grabs the foward slash diagonal of a tic-tac-toe board as a list
(defun grab-slash (list)
	(and (and (equal (first list) (fifth list))
		(equal (fifth list) (ninth list))) (not (eql '- (first list))))
)

;; Grabs the back slash diagonal of a tic-tac-toe board as a list
(defun grab-backslsh (list)
	(and (and (equal (third list) (fifth list))
		(equal (fifth list) (seventh list))) (not (eql '- (third list))))
)

;; Determines (evaluates to t or nil) whether or not there is a winner
;; by looking at the rows of the board.
;; Needs to make sure that the winner is a 'x or an 'o, not just '- character
(defun row-winner (board)
	(or (and (three-equal (grab-row board 0)) (not (eql '- (car (grab-row board 0)))))
		(and (three-equal (grab-row board 1)) (not (eql '- (car (grab-row board 1)))))
		(and (three-equal (grab-row board 2)) (not (eql '- (car (grab-row board 2))))))
)

;; Determines (evaluates to t or nil) whether or not there is a winner
;; by looking at the columns of the board.
;; Needs to make sure that the winner is a 'x or an 'o, not just '- character
(defun col-winner (board)
	(or (and (three-equal (grab-col board 0)) (not (eql '- (car (grab-col board 0)))))
		(and (three-equal (grab-col board 1)) (not (eql '- (car (grab-col board 1)))))
		(and (three-equal (grab-col board 2)) (not (eql '- (car (grab-col board 2)))))) 
)

;; places a move onto the board
;; move is the number indicating the place in the list.  
(defun place-move (board move type)
	(if (= move 0)
		(cons type (cdr board))
		(cons (car board) (place-move (cdr board) (- move 1) type))
	)
)

;; Function that keeps count of the number of moves.
(defun count-move (board)
	(if (null board)
	0
	(if (or (eql (car board) 'x) (eql (car board) 'o))
		(+ 1 (count-move (cdr board)))
		(count-move (cdr board))))
)

;; Determines the type of move that will be next
(defun deter-type (board)
	(if (= (mod (count-move board) 2) 0)
		'x
		'o)
)

;; Checks to see if board is full
(defun board-full (board)
	(not (member '- board))
)

;; Placement wrapper - places a move on the board, checks for places
;; within the board 
(defun place-move-wrap (board move)
	(if (and (< move (length board)) ;; Detects placing outside of board
		(eql (nth move board) '-))	 ;; Detects placing inside of board
		(place-move board move (deter-type board)) ;; a sub wrapper to get type
	nil)
)

;; This should check the many different ways to win.
(defun winner (board)
	(or (row-winner board)
		(col-winner board)
		(grab-slash board)
		(grab-backslsh board))
)

;; tests whether the move space is currently empty
;; move is a number 0 to 8 representing the nth element of the board list
;; We do this by making sure the move spot on the board is '- symbol
(defun legal-move (board)
	(format t "~%Please enter a move(0 through 8): ")
	(let ((position (read)))
		(cond ((not (and (integerp position)(<= 0 position 8)))
			(format t "~%Invalid input.")
			(legal-move board))
			((not (eql '- (nth position board)))
			(format t "~%Place is taken")
			(legal-move board))
			(t position)))
)

;; This function will randomly generate a random number between 0-9 (excluding 9)
;; It then checks to see if that number position on the board is open or not.
;; If that number position is taken, it will generate another random number and checks again
;; until it finds an open spot/position.
(defun random-empt-pos (board)
	(let ((pos (random 9)))
		(if (eql '- (nth pos board))
			pos
			(random-empt-pos board)))
)

;------------------------------------------------------------------
;;This set of code below is for player vs player. 

;; Checks to see if board is full
;; If not then it runs winner-x to check if there is a win.
(defun check-x (board)
	(if (board-full board)
		(format t "~%Tie game.")
		(winner-x board))
)

;; Checks to see if player x has won
;; If not, gives the move to player o
(defun winner-x (board)
	(if (winner board)
		(format t "~%Congrats! You won 1st player.")
		(po board))
)

;; Checks to see if board is full
;; If not then it runs winner-o to check if there is a win.
(defun check-o (board)
	(if (board-full board)
		(format t "~%Tie game.")
		(winner-o board))
)

;; Checks to see if player o has won
;; If not, gives the move to player x
(defun winner-o (board)
	(if (winner board)
		(format t "~%Congrats! You won 2nd player.")
		(px board))
)

;; This is for the player who is in control of x
(defun px (board)
	(let* ((nboard (place-move-wrap board (legal-move board))))
	(print-board nboard)
	(check-x nboard))
	(values)
)

;; This is for the player who is in control of o
(defun po (board)
	(let* ((nboard (place-move-wrap board (legal-move board))))
	(print-board nboard)
	(check-o nboard))
	(values)
)
;--------------------------------------------------------------------------
;;This set of code below is for player vs computer. Where player is x and computer is o.

;; Checks to see if board is full
;; If not then it runs win-px to check if x wins.
(defun check-px (board)
	(if (board-full board)
		(format t "~%Tie game.")
		(win-px board))
)

;; Checks to see if there is three in a row.
;; If player has does not have three in a row, then gives the move to the computer.
(defun win-px (board)
	(if (winner board)
		(format t "~%Congrats! Player has won.")
		(ai-move board))
)

;; Checks to see if board is full.
;; If not then it runs win-com to see if the computer has won.
(defun check-ai (board)
	(if (board-full board)
		(format t "~%Tie game.")
		(win-ai board))
)

;; Checks to see if there is three in a row.
;; If not then it gives the move to the player.
(defun win-ai (board)
	(if (winner board)
		(format t "~%Computer has won. Nice try player.")
		(p-vs-com board))
)

;; This is for the player who is in control of x, is used with function ai-move.
;; Used to play against the computer.
(defun p-vs-com (board)
	(let* ((nboard (place-move-wrap board (legal-move board))))
	(print-board nboard)
	(check-px nboard))
	(values)
)

;; This function implements computer move which plays as o.
;; Is used to play with p-vs-com.
(defun ai-move (board)
	(let* ((nboard (place-move-wrap board (random-empt-pos board))))
	(print-board nboard)
	(check-ai nboard))
	(values)
)

;-----------------------------------------------------------------------------
;;This set of code below is for player vs computer. Where computer is x and player is o.

;; Checks to see if board is full
;; If not then it runs win-aix to check if computer wins.
(defun check-aix (board)
	(if (board-full board)
		(format t "~%Tie game.")
		(win-aix board))
)

;; Checks to see if there is three in a row.
;; If player has does not have three in a row, then gives the move to the player.
(defun win-aix (board)
	(if (winner board)
		(format t "~%Computer has won. Nice try player.")
		(player-move board))
)

;; Checks to see if board is full.
;; If not then it runs win-com to see if the player has won.
(defun check-po (board)
	(if (board-full board)
		(format t "~%Tie game.")
		(win-po board))
)

;; Checks to see if there is three in a row.
;; If not then it gives the move to the computer.
(defun win-po (board)
	(if (winner board)
		(format t "~%Congrats! Player has won.")
		(com-vs-p board))
)

;; This is for the computer who is x, is used with function player-move.
;; Used to play against the computer.
(defun com-vs-p (board)
	(let* ((nboard (place-move-wrap board (random-empt-pos board))))
	(print-board nboard)
	(check-aix nboard))
	(values)
)

;; This function implements computer move which plays as o.
;; Is used to play with com-vs-p.
(defun player-move (board)
	(let* ((nboard (place-move-wrap board (legal-move board))))
	(print-board nboard)
	(check-po nboard))
	(values)
)

;-----------------------------------------------------------------------------

;;This set of code below is for computer vs computer.

;; Checks to see if board is full.
;; If not then runs win-ai-x.
(defun check-ai-x (board)
	(if (board-full board)
		(format t "~%Tie game.")
		(win-ai-x board))
)

;; Checks to see if there is three in a row.
;; If not then it gives the move to computer o.
(defun win-ai-x (board)
	(if (winner board)
		(format t "~%Computer x has won.")
		(ai-vs-ai-o board))
)

;; Checks to see if board is full.
;; if ot then runs win-ai-o.
(defun check-ai-o (board)
	(if (board-full board)
		(format t "~%Tie game.")
		(win-ai-o board))
)

;; Checks to see if there is three in a row.
;; If not then gives the move to computer x.
(defun win-ai-o (board)
	(if (winner board)
		(format t "~%Computer o has won.")
		(ai-vs-ai-x board))
)

;;
(defun ai-vs-ai-x (board)
	(let* ((nboard (place-move-wrap board (random-empt-pos board))))
		(print-board nboard)
		(check-ai-x nboard))
		(values)
)

;;
(defun ai-vs-ai-o (board)
	(let* ((nboard (place-move-wrap board (random-empt-pos board))))
		(print-board nboard)
		(check-ai-o nboard))
		(values)
)

;-----------------------------------------------------------------------------

;; This function sees what mode the user wants to play.
(defun play-mode ()
	(format t "~%Press 1 to play against another player.")
	(format t "~%Press 2 to play against the computer. ")
	(if (eql '1 (read))
		(px (make-board))
		(against-ai))
)

;; This function sees if the user wants to be x or be o.
(defun against-ai ()
	(format t "~%Press 1 to have you be x and computer to be o.")
	(format t "~%Press 2 to have computer be x and you be o. ")
	(if (eql '1 (read))
		(p-vs-com (make-board))
		(com-vs-p (make-board)))
)

;; Implements a game of Tic Tac Toe
(defun play ()
	(print-board (make-board))
	(format t "~%If you would like to play against another player or a computer enter 1.")
	(format t "~%If you would like to watch the computer play itself then press 2. ")
	(if (eql '1 (read))
		(play-mode)
		(ai-vs-ai-x (make-board)))
)
